/**
 * @file Robot1
 * The first robot in relay.
 *
 * @author TJArk
 */

option(Robot1)
{
	float p = -0.9f;
    common_transition
    {
    	if (libCodeRelease.odometryXSum >= 3100.f)
			theBehaviorStatus.firstRobotArrived = true;
		else
			theBehaviorStatus.firstRobotArrived = false;
    }

    initial_state(walkStraight)
    {
        transition
        {
        	if(libCodeRelease.odometryXSum >= 3500.f)
        		goto finish;
        }
        action
        {
        	WalkAtSpeedPercentage(Pose2f(p * libCodeRelease.odometryRSum, 0.8f, 0.f));
        }
    }

    state(finish)
    {
    	transition
		{

		}
    	action
		{
    		Stand();
		}
    }
}


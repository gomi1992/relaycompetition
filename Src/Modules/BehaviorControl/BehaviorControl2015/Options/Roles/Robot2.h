/**
 * @file Robot2
 * The second robot in relay.
 *
 * @author TJArk
 */

option(Robot2)
{ 
	float p = -0.9f;
	common_transition
	{

	}

	initial_state(stand)
	{
		transition
		{
		  if(!theTeammateData.teammates.empty() && theTeammateData.teammates[0].firstRobotArrived)
			  goto walkStraight;
		}
		action
		{
			Stand();
		}
	}

	state(walkStraight)
	{
	   transition
	   {
		  if(libCodeRelease.odometryXSum >= 3500.f)
			 goto finish;
	    }
		action
		{
			WalkAtSpeedPercentage(Pose2f(p * libCodeRelease.odometryRSum, 0.8f, 0.f));
		}
	}

	state(finish)
	{
		transition
		{

		}
		action
		{
			Stand();
		}
	}
}
